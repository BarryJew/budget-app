import Vue from "vue";

const balanceStore = {
    namespaced: true,
    state: {
        list: {
            1: {
                type: "INCOME",
                value: 100,
                comment: "Some comment",
                id: 1,
            },
            2: {
                type: "OUTCOME",
                value: 50,
                comment: "Some outcome comment",
                id: 2,
            },
        },
    },
    getters: {
        balanceList: ({ list }) => list

    },
    mutations: {
        ADD_ITEM({ list }, item) {
            Vue.set(list, item.id, item);
        },
        DELETE_ITEM({ list }, id) {
            Vue.delete(list, id);
        }
    },
    actions: {
        addBalanceItem({commit}, balanceItem) {
            const newObject = {
                ...balanceItem,
                id: String(Math.random())
            };
            commit("ADD_ITEM", newObject);
        },
        deleteBalanceItem({commit}, id) {
            commit("DELETE_ITEM", id);
        }
    }
}

export default balanceStore;